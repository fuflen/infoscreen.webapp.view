import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {TemplateService} from '../../Services/template.service';
import * as signalR from "@aspnet/signalr";
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import {environment} from '../../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {ViewHubClientService} from '../../Services/view-hub-client.service';
import {Observable} from 'rxjs';
import {Message} from '../../Models/Message';

@Component({
  selector: 'app-info-screen',
  templateUrl: './info-screen.component.html',
  styleUrls: ['./info-screen.component.css']
})
export class InfoScreenComponent implements OnInit, OnDestroy {

  public htmlTemplate : string = null;
  public _infoscreenId : number;

  private subscription : any;


  name = 'Angular';
  @HostListener('window:beforeunload')
  ResetPage() {
    this.viewHubClientService.RemoveInfoScreen(this._infoscreenId);
    if(this.subscription != null) {
      this.subscription.unsubscribe();
    }
  }
  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    // ...
  }

  constructor(private templateService : TemplateService,
              private route : ActivatedRoute,
              private viewHubClientService : ViewHubClientService) { }

  ngOnInit() {

    let id = this.route.snapshot.params["infoScreenId"];
    console.log(id);
    this._infoscreenId = Number(id);

    this.listenForConnection();
    if(!isNaN(id)) {
      this.viewHubClientService.StartConnection();
    }
    this.listenForChanges();
  }

  listenForConnection()
  {
    this.subscription = this.viewHubClientService.listenForConnectionStarted().subscribe(isStarted =>
    {
      this.viewHubClientService.AddNewInfoScreenToGroup(this._infoscreenId);
    });
  }

  listenForChanges() {

    this.subscription = this.viewHubClientService.listenToMessages().subscribe(message =>
    {

      console.log(message);
      this.htmlTemplate = message.content
    });
  }

  ngOnDestroy() {
    this.viewHubClientService.RemoveInfoScreen(this._infoscreenId);
    if(this.subscription != null) {
      this.subscription.unsubscribe();
    }
  }





















  /*this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(environment.scheduleEndpointUrl + '/viewhub')
      .build();

    this.hubConnection
      .start()
      .then(() => {console.log('Connection started');

      })
      .catch(err => console.log('Error while starting connection: ' + err));

    this.hubConnection.on('ShowView', (data) => {
      console.log(data);
    });*/


  /*this.anotherHubConnection = new signalR.HubConnectionBuilder()
    .withUrl(environment.scheduleEndpointUrl + '/infoscreenhub')
    .build();

  this.anotherHubConnection
    .start()
    .then(() => {console.log('Connection started');
      this.anotherHubConnection.send('newinfoscreen', this._infoscreenId)
        //.invoke('newinfoscreen', "I am nice screen number 1")
        .catch(err => console.error(err));
    })
    .catch(err => console.log('Error while starting connection: ' + err))*/

  /*
      let builder = new HubConnectionBuilder();
      this.viewHubConnection = builder.withUrl(environment.scheduleEndpointUrl + '/viewhub').build();  // see startup.cs
      this.viewHubConnection.on('ShowView', (message) => {
        this.messages.push(message);
        console.log(message);
      });
      this.viewHubConnection.start();*/


    /*this._signalR.connect().then((c) => {
      let onMessageSent$ = c.listenFor('ShowView');
      this.subscription = onMessageSent$.subscribe( x => {console.log("I GOT IT MOM! "); console.log(x)})
    });*/

  //}




}
