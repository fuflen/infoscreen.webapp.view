import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from "../Models/Result";
import {TemplateDTO} from "../Models/Template";

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  private url : string = "http://localhost:5000/GetPopulatedTemplate";
  constructor(private httpClient : HttpClient) {

  }

  public GetTemplate() : Observable<Result<TemplateDTO>>
  {

    let headers = new HttpHeaders( {
      'Content-Type': 'application/json; charset=utf-8'
    } );
    return this.httpClient.get<Result<TemplateDTO>>(this.url + "/1/1/10" , {headers: headers});
  }
}


