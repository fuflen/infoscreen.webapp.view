import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import {environment} from '../../environments/environment';
import {HubConnection} from '@aspnet/signalr';
import {Message} from '../Models/Message';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViewHubClientService {



  public viewHubConnection: HubConnection;
  message = new Subject<Message>();

  startingSubject = new Subject<boolean>();

  constructor() {
    this.viewHubConnection = new signalR.HubConnectionBuilder()
      .withUrl(environment.scheduleEndpointUrl + '/viewhub')
      .build();


    this.viewHubConnection.on('ShowView', ( data) => {
      this.message.next({ user: "user", content: data });
    });

    this.viewHubConnection.onclose(() => {
      this.StartConnection();
    });
  }

  public listenToMessages() : Observable<Message>
  {
    return this.message.asObservable();
  }

  public listenForConnectionStarted() : Observable<boolean>
  {
    return this.startingSubject.asObservable();
  }

  public AddNewInfoScreenToGroup(infoScreenId : Number)
  {
    console.log("Sending shit")
    this.viewHubConnection.send('newinfoscreen', infoScreenId)
      .catch(err => console.error(err));
  }

  public StartConnection()
  {
    let  self = this;

    this.viewHubConnection

      .start()
      .then(() => {console.log('Connection started');
        this.startingSubject.next(true);
      })
      .catch(err => setTimeout(function()
      {
        self.StartConnection();
      },3000));

  }

  public RemoveInfoScreen(infoScreenId : number)
  {
    this.viewHubConnection.send('RemoveInfoScreen', infoScreenId)
      .catch(err => console.error(err));
  }


}
