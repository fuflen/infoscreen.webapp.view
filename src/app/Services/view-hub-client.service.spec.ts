import { TestBed } from '@angular/core/testing';

import { ViewHubClientService } from './view-hub-client.service';

describe('ViewHubClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewHubClientService = TestBed.get(ViewHubClientService);
    expect(service).toBeTruthy();
  });
});
