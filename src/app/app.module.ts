import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {safeHtmlPipe} from '../pipes/safeHtmlPipe';
import { InfoScreenComponent } from './Components/info-screen/info-screen.component';
import { HtmlScreenComponent } from './Components/info-screen/Screens/html-screen/html-screen.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TemplateService} from './Services/template.service';
import {SignalRConfiguration, SignalRModule} from 'ng2-signalr';
import {environment} from '../environments/environment';
import {RouterModule, Routes} from '@angular/router';
import {ViewHubClientService} from './Services/view-hub-client.service';

export function createConfig(): SignalRConfiguration {
  const c = new SignalRConfiguration();
  c.hubName = '/viewhub';
  c.qs = { user: 'donald' };
  c.url = environment.scheduleEndpointUrl;
  c.logging = true;
  return c;
}

const routes : Routes = [
  {path: ":infoScreenId", component: InfoScreenComponent}
];



@NgModule({
  declarations: [
    AppComponent,
    safeHtmlPipe,
    InfoScreenComponent,
    HtmlScreenComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    SignalRModule.forRoot(createConfig)
  ],
  providers: [HttpClient,
    TemplateService,
    ViewHubClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
